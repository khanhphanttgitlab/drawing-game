# Drawing Game
Draw objects directly on your webcam

**1. Install necessary packages**
```
pip install -r requirements.txt
```

**2. Clone or download this project**

If you want to run the game directly, move to step 5. 

This pretrained model is for 10 classes: "apple", "bird", "boomerang", "cup", "ice cream", "leaf", "mug", "shovel", "star", "t-shirt"

**3. Select and download data you prefer**

https://console.cloud.google.com/storage/browser/quickdraw_dataset/full/numpy_bitmap

Remember to put all data in folder "data"

Dataset description: https://github.com/googlecreativelab/quickdraw-dataset

**4. Load data and train model.**
```
python loadTrainModel.py
```
Please wait until the training is done.

**5. Run the game with trained model**
```
python DrawingGame.py
```

